﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPPAssJustChess.Figures
{
    using Common;
    using OPPAssJustChess.Figures.Contracts;
    public class Rook : BaseFigure, IFigure
    {
        public Rook(ChessColor color) : base(color)
        {
        }
    }
}
