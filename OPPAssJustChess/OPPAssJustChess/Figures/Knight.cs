﻿

namespace OPPAssJustChess.Figures
{
    using Common;
    using OPPAssJustChess.Figures.Contracts;

    public class Knight : BaseFigure, IFigure
    {
        public Knight(ChessColor color) : base(color)
        {

        }
    }
}
