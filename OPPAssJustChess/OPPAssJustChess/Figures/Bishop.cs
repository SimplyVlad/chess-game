﻿


namespace OPPAssJustChess.Figures
{
    using Common;
    using OPPAssJustChess.Figures.Contracts;
    public class Bishop : BaseFigure, IFigure
    {
        public Bishop(ChessColor color) : base(color)
        {
        }
    }
}
