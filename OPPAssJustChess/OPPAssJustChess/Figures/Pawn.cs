﻿

namespace OPPAssJustChess.Figures
{
    using OPPAssJustChess.Common;
    using OPPAssJustChess.Figures.Contracts;
    public class Pawn : BaseFigure, IFigure
    {
        public Pawn(ChessColor color) : base(color)
        {

        }

    }
}
