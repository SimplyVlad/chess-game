﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPPAssJustChess.Figures
{
    using Common;
    using OPPAssJustChess.Figures.Contracts;
    public class Queen : BaseFigure, IFigure
    {
        public Queen(ChessColor color) : base(color)
        {
        }
    }
}
