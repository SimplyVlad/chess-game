﻿using OPPAssJustChess.Common;

namespace OPPAssJustChess.Figures.Contracts
{
   public interface IFigure
    {
        ChessColor Color { get; }
    }
}
