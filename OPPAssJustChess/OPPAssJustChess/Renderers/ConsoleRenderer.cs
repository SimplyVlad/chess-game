﻿


namespace OPPAssJustChess.Renderers
{
    using System;
    using Board.Contracts;
    using OPPAssJustChess.Renderers.Contracts;
    using System.Threading;
    using Common.Console;

    public class ConsoleRenderer : IRenderer
    {
        private const string Logo = "JUST CHESS";
        private const int CharactersPerRowPerBoardSquare = 9;
        private const int CharactersPerColPerBoardSquare = 9;
        public void RenderBoard(IBoard board)
        {
            //TODO: validate Console dimensions
            var startRowPrint = Console.WindowHeight / 2 - (board.TotalRows / 2) * CharactersPerRowPerBoardSquare;
            var startColPrint = Console.WindowWidth / 2 - (board.TotalCols / 2) * CharactersPerColPerBoardSquare;

            var currentRowPrint = startRowPrint;
            var currentColPrint = startColPrint;

            Console.BackgroundColor = ConsoleColor.White;
            for (int top  = 0; top < board.TotalRows; top++)
            {
                for (int left = 0; left < board.TotalCols; left++)
                {
                    currentRowPrint = startRowPrint + top * CharactersPerRowPerBoardSquare;
                    currentColPrint = startColPrint + left * CharactersPerColPerBoardSquare;

                    Console.SetCursorPosition(currentColPrint, currentRowPrint);
                    Console.Write(" ");
                }

            }

            

            
     

            Console.ReadLine();
        }

        public void RenderMainMenu()
        {
            ConsoleHelpers.SetCursorAtCenter(Logo.Length);
            Console.WriteLine(Logo);

            //TODO: add main menu
            Thread.Sleep(1000);

            Console.WriteLine(Logo);
        }
    }
}
