﻿


namespace OPPAssJustChess.Renderers.Contracts
{
    using OPPAssJustChess.Board.Contracts;
    public interface IRenderer
    {
        void RenderMainMenu();
        void RenderBoard(IBoard board);

    }
}
