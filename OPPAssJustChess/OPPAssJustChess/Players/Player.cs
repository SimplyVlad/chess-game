﻿

namespace OPPAssJustChess.Players
{
    using Contracts;
    using System.Collections.Generic;
    using OPPAssJustChess.Figures.Contracts;
    using Common;
    using System;
    

    public class Player : IPlayer
    {
        private readonly ICollection<IFigure> figures;

        public string Name { get; private set; }

        public ChessColor Color { get; private set; }

        public Player(ChessColor color, string name)
        {
            this.figures = new List<IFigure>();
            this.Color = color;
            this.Name = name;
            //TODO: check figure and player color
        }

        public void AddFigure(IFigure figure)
        {
            ObjectValidator.CheckIfObjectIsNull(figure, GlobalErrorMessages.NullErrorMessage);
            CheckIfFigureExists(figure);
            this.figures.Add(figure);
        }
        //TODO: check figure and player color
        public void RemoveFigure(IFigure figure)
        {
            ObjectValidator.CheckIfObjectIsNull(figure, GlobalErrorMessages.NullErrorMessage);
            CheckIfFigureDoesNotExist(figure);
            this.figures.Remove(figure);
        }

        private void CheckIfFigureExists(IFigure figure)
        {
            if(this.figures.Contains(figure))
            {
                throw new InvalidOperationException("This player already owns this figure!");
            }
        }
        private void CheckIfFigureDoesNotExist(IFigure figure)
        {
            if (!this.figures.Contains(figure))
            {
                throw new InvalidOperationException("This player does not own this figure!");
            }
        }
    }
}
