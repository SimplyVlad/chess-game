﻿


namespace OPPAssJustChess.Players.Contracts
{
    using OPPAssJustChess.Common;
    using OPPAssJustChess.Figures.Contracts;
    public interface IPlayer
    {
        string Name { get; }
        ChessColor Color
        {
            get;
        }
        void AddFigure(IFigure figure);

        void RemoveFigure(IFigure figure);
    }
}
