﻿

namespace OPPAssJustChess.Common
{
 public class GlobalConstants
    {
        public const int StandardGameNumberOfPlayers = 2;

        public const int StandardGameTotalBoardRows = 8;
        public const int StandardGameTotalBoardCols = 8;

        public const int MinimumRowValueOnBoard = 1;
        public const int MaximumRowValueOnBoard = 8;

        public const char MinimumColValueOnBoard = 'a';
        public const char MaximumColValueOnBoard = 'h';

        public const string EmptyString = "";
    }
}
