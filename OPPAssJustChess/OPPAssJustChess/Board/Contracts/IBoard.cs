﻿



namespace OPPAssJustChess.Board.Contracts
{
    using OPPAssJustChess.Common;
    using OPPAssJustChess.Figures.Contracts;
    public interface IBoard
    {
        int TotalRows
        {
            get;
        }

        int TotalCols
        {
            get;
        }

        void AddFigure(IFigure figure, Position position);

        void RemoveFigure(Position position);
    }
}
