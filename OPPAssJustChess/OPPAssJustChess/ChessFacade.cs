﻿


namespace OPPAssJustChess
{
    using Engine;
    using Engine.Contracts;
    using Engine.Initializations;
    using InputProviders;
    using InputProviders.Contracts;
    using OPPAssJustChess.Renderers.Contracts;
    using Renderers;
    using System;
    public static class ChessFacade
    {

        public static void Start()
        {
            IRenderer renderer = new ConsoleRenderer();
            //renderer.RenderMainMenu();

            IInputProvider inputProvider = new ConsoleInputProvider();

            IChessEngine chessEngine = new StandardTwoPlayerEngine(renderer, inputProvider);

            IGameInitializationStrategy gameInitializationStrategy = new StandardStartGameInitializationStrategy();

            chessEngine.Initialize(gameInitializationStrategy);
            chessEngine.Start();

            Console.ReadLine();
        }
    }
}
