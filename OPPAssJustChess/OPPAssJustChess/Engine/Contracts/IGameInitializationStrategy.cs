﻿



namespace OPPAssJustChess.Engine.Initializations
{
    using Board.Contracts;
    using OPPAssJustChess.Players.Contracts;
    using System.Collections.Generic;
    public  interface IGameInitializationStrategy
    {
        void Initialize(IList<IPlayer> players, IBoard board );
        
    }
}
