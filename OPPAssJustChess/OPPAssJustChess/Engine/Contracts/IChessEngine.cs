﻿



namespace OPPAssJustChess.Engine.Contracts
{
    using Initializations;
    using OPPAssJustChess.Players.Contracts;
    using System.Collections.Generic;
    public interface IChessEngine
    {
        IEnumerable<IPlayer> Players { get; }
        void Initialize(IGameInitializationStrategy gameInitializationStrategy);

        void Start();

        void WinningCondition();
    }
}
