﻿



namespace OPPAssJustChess.Engine.Initializations
{
    using System;
    using System.Collections.Generic;
    using OPPAssJustChess.Board.Contracts;
    using OPPAssJustChess.Players.Contracts;
    using OPPAssJustChess.Figures;
    using OPPAssJustChess.Common;
    using Figures.Contracts;

    class StandardStartGameInitializationStrategy : IGameInitializationStrategy
    {
       
        private const int BoardTotalRowsAndCols = 8;

        private IList<Type> figureTypes;

        public StandardStartGameInitializationStrategy()
        {
            this.figureTypes = new List<Type>
            {
                typeof(Rook),
                typeof(Knight),
                typeof(Bishop),
                typeof(Queen),
                typeof(King),
                typeof(Bishop),
                typeof(Knight),
                typeof(Rook),
            };
        }
        public void Initialize(IList<IPlayer> players, IBoard board)
        {
            this.ValidateStrategy(players, board);

            var firstPlayer = players[0];
            var secondPlayer = players[1];

            this.AddArmyTOBoardRow(firstPlayer, board, 8);
            this.AddPawnsToBoardRow(firstPlayer, board, 7);

            this.AddArmyTOBoardRow(secondPlayer, board, 1);
            this.AddPawnsToBoardRow(secondPlayer, board, 2);
        }

        private void AddPawnsToBoardRow(IPlayer player, IBoard board, int chessRow)
        {
            for (int i = 0; i < BoardTotalRowsAndCols; i++)
            {
                var pawn = new Pawn(player.Color);
                player.AddFigure(pawn);
                board.AddFigure(pawn, new Position(chessRow, (char)(i + 'a')));
            }

        }

        private void AddArmyTOBoardRow(IPlayer player, IBoard board, int chessRow)
        {
            for (int i = 0; i < BoardTotalRowsAndCols; i++)
            {
                var figureType = this.figureTypes[i];
                var figureInstance = (IFigure)Activator.CreateInstance(figureType, player.Color);
                player.AddFigure(figureInstance);
                board.AddFigure(figureInstance, new Position(chessRow, (char)(i + 'a')));
            }
        }

        private void ValidateStrategy(IList<IPlayer> players, IBoard board)
        {
            if (players.Count != GlobalConstants.StandardGameNumberOfPlayers)
            {
                throw new InvalidOperationException("Standard Start Game Initialization Strategy needs exactly two players!");

            }

            if (board.TotalRows != BoardTotalRowsAndCols || board.TotalCols != BoardTotalRowsAndCols)
            {
                throw new InvalidOperationException("Standard Start Game Initialization Strategy needs exactly 8x8 board!");
            }
        }


    }
}
