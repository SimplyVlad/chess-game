﻿



namespace OPPAssJustChess.Engine
{
    using System;
    using OPPAssJustChess.Engine.Contracts;
    using System.Collections.Generic;
    using OPPAssJustChess.Players.Contracts;
    using Initializations;
    using System.Collections;
    using Board.Contracts;
    using Board;
    using Renderers.Contracts;
    using InputProviders.Contracts;
    using Common;
    using Players;

    public class StandardTwoPlayerEngine : IChessEngine
    {
        private readonly IEnumerable<IPlayer> players;
        private readonly IRenderer renderer;
        private readonly IInputProvider input;
        private readonly IBoard board;

        public StandardTwoPlayerEngine(IRenderer renderer, IInputProvider inputProvider)
        {
            this.renderer = renderer;
            this.input = inputProvider;
            this.board = new Board();

        }
        public void Initialize(IGameInitializationStrategy gameInitializationStrategy)
        {

            //TODO: remove using JustChess.Players and use the input for players
            var players = new List<IPlayer>
            {
                new Player(ChessColor.Black, "Pesho"),
                new Player(ChessColor.White, "Gosho")
            };
                
                
                //this.input.GetPlayers(GlobalConstants.StandardGameNumberOfPlayers);
            gameInitializationStrategy.Initialize(players, board);
            this.renderer.RenderBoard(board);
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void WinningCondition()
        {
            throw new NotImplementedException();
        }

        public void Initialize()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IPlayer> Players {
            get
            {
                return new List<IPlayer>(this.players);
             }
        }

    }
}
