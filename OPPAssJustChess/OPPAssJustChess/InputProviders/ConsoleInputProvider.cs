﻿


namespace OPPAssJustChess.InputProviders
{
    using Common;
    using Common.Console;
    using Contracts;
    using OPPAssJustChess.Players.Contracts;
    using Players;
    using System;
    using System.Collections.Generic;
    public class ConsoleInputProvider : IInputProvider
    {
        private const string PlayerNameText = "Enter Player {0} name: ";
        public IList<IPlayer> GetPlayers(int numberOfPlayers)
        {
            var players = new List<IPlayer>();

            for(int i =1; i< numberOfPlayers; i++)
            {
                Console.Clear();
                ConsoleHelpers.SetCursorAtCenter(PlayerNameText.Length);
                Console.Write(string.Format(PlayerNameText, i));
                string name = Console.ReadLine();
                var player = new Player((ChessColor)(i - 1), name);
                players.Add(player);
            }
            return players;
        }
    }
}
