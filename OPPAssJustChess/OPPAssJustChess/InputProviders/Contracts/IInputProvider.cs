﻿



namespace OPPAssJustChess.InputProviders.Contracts
{
    using Players.Contracts;
    using System.Collections.Generic;
    public interface IInputProvider
    {
        IList<IPlayer> GetPlayers(int numberOfPlayers);
    }
}
